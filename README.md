# Primeoffice

Primeoffice is a powerfull tool for backoffice management. With our solution you'll be able to configure your models to be directly used in a completly ready-to-use backoffice.

## Installation

Simply use composer for install Primeoffice

```Bash
composer require launchit/primeoffice
```

After that you have to publish the config file in your project

```Bash
php artisan vendor:publish --tag=primeoffice
```

Simply run a migrate and you're ready to start configuring your backoffice

```Bash
php artisan migrate
```

## Config file

An only config file is provide by primeoffice. Inside you can find an exemple of a simple configuration which integrates the User model in your backoffice. Your free to modify it as often as you like to create something great ;)

```PHP
return [
    'models' => [
        'User' => \App\Models\User::class,
    ],

    'fields' => [
        'User' => [
            'name' => 'string',
            'is_admin' => 'bool',
            'email' => 'string',
            'password' => 'crypt'
        ],
    ],

    'display' => [
        'User' => [
            'name' => 'Name',
            'is_admin' => 'Admin',
            'email' => 'E-mail',
            'password' => 'Mot de passe'
        ],
    ],

    'modes' => [
        'User' => 'crud',
    ],

    'apparences' => [
        'User' => '<i class="fas fa-users"></i>',
    ]
];
```

### models
Refers to the models class concerned by the backoffice.

### fields
Refers the database fields that can be modified in the backoffice.

### display
The fields to be displayed in the row list.

### modes
describes the permissions allocated to the user on this model.

### apparences
The fontawesome's icons to be displaied on model representation.

## Migration

Primeoffice include the most simple migration you ever see. It only add one "is_admin" boolean field to the users table that allows you to manage what kind of users can access the backoffice.

```PHP
$this->tyniInteger('is_admin')->default(0);
```

That field work with the middleware, it's set at 0 for a standard user and at 1 for an administrator who can see your backoffice 

## Routing

Configured out of the box, you have one only simple route for access your backoffice view, configured with the admin middleware, is name is backoffice.home.

## Middleware

Inside the package you can found the "auth.admin" middleware that secures the backoffice access. It check if the field is_admin, added by the migration, is true for the current user. You're free to use it anywhere you whant.

## Pretty way with your models

You can add the config entries for each of your models directly into them using class constants. For the previous config file we can write in the User model

```PHP
    /**
     * Global access mode for back-office
     *
     * @var array
     */
    public const BACKOFFICE_MODE = 'ru';

    /**
     * Enabled field in back-office
     *
     * @var array
     */
    public const BACKOFFICE_FIELD = [
        'is_admin' => 'bool',
        'name' => 'string',
        'email' => 'string',
    ];
    
    /**
     * Displayable field in back-office
     *
     * @var array
     */
    public const BACKOFFICE_DISPLAY = [
        'is_admin' => 'Admin',
        'name' => 'Nom',
        'email' => 'E-mail',
    ];
```
And simply add its values to the config

```PHP
'fields' => [
    'User' => User::BACKOFFICE_FIELD
]
```


