<?php

use Illuminate\Support\Facades\Route;

Route::get('backoffice', \Primeoffice\Http\Livewire\BackOffice::class)->name('backoffice.home')->middleware('web')->middleware('auth.admin');


