<?php

return [
    'models' => [
        'User' => \App\Models\User::class,
    ],

    'fields' => [
        'User' => [
            'name' => 'string',
            'is_admin' => 'bool',
            'email' => 'string',
            'password' => 'crypt'
        ],
    ],

    'display' => [
        'User' => [
            'name' => 'Name',
            'is_admin' => 'Admin',
            'email' => 'E-mail',
            'password' => 'Mot de passe'
        ],
    ],

    'modes' => [
        'User' => 'crud',
    ],

    'apparences' => [
        'User' => '<i class="fas fa-users"></i>',
    ]
];
