<div>
    <x-jet-dropdown>
        <x-slot name="trigger">
            <x-jet-button class="py-4 text-white bg-gray-700 rounded-md hover:text-black"><i class="fas fa-filter"></i></x-jet-button>
        </x-slot>

        <x-slot name="content">
            <div>
                <x-jet-dropdown-link class="w-full" wire:click="setFilter">Aucun filtres</x-jet-dropdown-link>
                @foreach($display as $field => $val)
                    <x-jet-dropdown-link wire:click="setFilter('{{ $field }}')">
                        <div class="flex items-center justify-between">
                            {{ $val }}
                            @if($field == $filter)
                                @if($direction == 'asc') <i class="fas fa-sort-up"></i>
                                @else <i class="fas fa-sort-down"></i>
                                @endif
                            @endif
                        </div>
                    </x-jet-dropdown-link>
                @endforeach
            </div>
        </x-slot>
    </x-jet-dropdown>
</div>
