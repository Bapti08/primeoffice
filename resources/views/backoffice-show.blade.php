<div>
    @isset($content->items()[0])
        <div class="p-5 border-4 border-gray-200 border-dashed rounded-lg">
            <table class="min-w-full">
                <thead>
                    <tr>
                        @foreach($display as $key => $val)
                            <th wire:key='head-{{ $model }}-{{ $key }}' class="px-6 py-3 text-sm leading-4 tracking-wider text-left text-blue-500 border-b-2 border-gray-300">{{ $val }}</th>
                        @endforeach
                        <th class="px-6 py-3 text-sm leading-4 tracking-wider text-left text-blue-500 border-b-2 border-gray-300">Opérations</th>
                    </tr>
                </thead>
                <tbody class="bg-white">
                    @foreach($content as $key => $row)
                        <tr wire:key='row-{{ $model }}-{{ $loop->index }}' class="border-b border-gray-500">
                            @foreach($fields as $key => $type)
                                @if(isset($display[$key]) && isset($row->toArray()[$key]))
                                    <td wire:key='entry-{{ $model }}-{{ $loop->index }}' class="px-6 py-4 text-sm leading-5 text-blue-900 whitespace-no-wrap ">
                                        @if($type == 'bool')
                                            @if(boolval($row->$key)) <i class="text-green-600 far fa-check-circle"></i>
                                            @else <i class="text-red-600 far fa-times-circle"></i>
                                            @endif
                                        @elseif($type == 'color')
                                            <i class="text-{{ $row->toArray()[$key] }} fas fa-circle"></i>
                                        @elseif((explode('.', $type)[0] == 'string' || explode('.', $type)[0] == 'text') && strlen($row->toArray()[$key]) > 20)
                                            {{ substr($row->toArray()[$key], 0, 50) }}
                                            @if (strlen($row->toArray()[$key] > 50))
                                                ...
                                            @endif
                                        @elseif(explode('.', $type)[0] == 'id')
                                            {{ $models[explode('.', $type)[1]]::find($row->toArray()[$key])->name }}
                                        @elseif($type == 'date')
                                            {{ \Carbon\Carbon::create($row->toArray()[$key])->format('d-m-Y') }}
                                        @elseif($type == 'crypt')
                                            @for($i = 0; $i < 10; $i++)
                                                &bull;
                                            @endfor
                                        @else
                                            {{ $row->toArray()[$key] }}
                                        @endif
                                    </td>
                                @endif
                            @endforeach
                            <td class="flex justify-end gap-3 px-6 py-4 text-sm leading-5 text-right text-blue-900 whitespace-no-wrap">
                                @if (strpos($this->modes, 'u') !== false)
                                    <button wire:click="update({{ $row->id }})"><i class="fas fa-edit"></i></button>
                                @endif
                                @if (strpos($this->modes, 'd') !== false)
                                    <button wire:click='delete({{ $row->id }})'><i class="fas fa-trash-alt"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endisset

    <div class="mt-4">
        {{ $content->links() }}
    </div>
</div>
