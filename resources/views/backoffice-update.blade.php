<div>

    <x-jet-modal wire:model='update_modal'>
        <div class="py-3">
            <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
                <div class="py-4">
                    @php
                        $namespace = explode('\\', $model);
                        $name = $namespace[array_key_last($namespace)];
                    @endphp
                    <h1 class="mb-5 text-2xl font-semibold text-center text-gray-900">{{ $name }}</h1>
                    <div class="p-5 border-4 border-gray-200 border-dashed rounded-lg">
                        <form class="m-4" wire:submit.prevent='update'>
                            @foreach($fields as $name => $field)
                            <div class="w-full mb-3" wire:key='update-{{ $name }}'>
                                <x-jet-label class="capitalize" for="{{ $name }}" value="{{ isset($display[$name]) ? $display[$name] : $name }}" />
                                @php
                                    $rule = explode('.', $field);
                                @endphp

                                @switch($rule[0])
                                    @case('string')
                                        <x-jet-input class="w-full" wire:model.defer="update_fields.{{ $name }}" placeholder="{{ isset($placeholders[$name]) ? $placeholders[$name] : '' }}" type="text" id="{{ $name }}" />
                                    @break

                                    @case('text')
                                        <textarea class="w-full" wire:model.defer="update_fields.{{ $name }}" placeholder="{{ isset($placeholders[$name]) ? $placeholders[$name] : '' }}" id="{{ $name }}"></textarea>
                                    @break

                                    @case('int')
                                        <x-jet-input class="w-full" wire:model.defer="update_fields.{{ $name }}" placeholder="{{ isset($placeholders[$name]) ? $placeholders[$name] : '' }}" type="number" step="1" id="{{ $name }}" />
                                    @break

                                    @case('float')
                                        <x-jet-input class="w-full" wire:model.defer="update_fields.{{ $name }}" placeholder="{{ isset($placeholders[$name]) ? $placeholders[$name] : '' }}" type="number" step="0.01" id="{{ $name }}" />
                                    @break

                                    @case('bool')
                                        <x-primeoffice::toggle-button class="w-full" wire:model.defer="update_fields.{{ $name }}" id="{{ $name }}"
                                        wire:value="{{(isset($placeholders[ 'is_admin' ])) ? $placeholders[ 'is_admin' ] : 0}}"
                                        />
                                    @break

                                    @case('select')
                                        <select class="w-full" wire:model.defer="update_fields.{{ $name }}" id="{{ $name }}">
                                            @foreach($select_fields as $val)
                                                <option value="{{ $val }}" class="capitalize">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    @break

                                    @case('file')
                                        <x-jet-input id="{{ $name }}" type="file" />
                                    @break

                                    @case('id')
                                        @php
                                            $select_fields = $models[$rule[1]]::select('id', 'name')->get();
                                            $this->update_fields[$name] = $select_fields->first()->id;
                                        @endphp

                                        <select class="w-full" wire:model.defer="update_fields.{{ $name }}" id="{{ $name }}">
                                            @foreach($select_fields as $val)
                                                <option value="{{ $val->id }}" class="capitalize">{{ $val->name }}</option>
                                            @endforeach
                                        </select>
                                    @break;

                                    @case('color')
                                        @php
                                            $this->update_fields[$name] = 'indigo-400';
                                        @endphp

                                        <select class="w-full" wire:model.defer="update_fields.{{ $name }}" id="{{ $name }}">
                                            <option value="indigo-400" class="capitalize">Indigo</option>
                                            <option value="purple-400" class="capitalize">Rose</option>
                                            <option value="yellow-400" class="capitalize">Jaune</option>
                                            <option value="blue-400" class="capitalize">Bleu</option>
                                            <option value="red-400" class="capitalize">Rouge</option>
                                            <option value="green-400" class="capitalize">Vert</option>
                                        </select>
                                    @break

                                    @case('crypt')
                                        <x-jet-input class="w-full" wire:model.defer="update_fields.{{ $name }}" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;" type="password" id="{{ $name }}" />
                                    @break

                                    @default
                                        <x-jet-input class="w-full" wire:model.defer="update_fields.{{ $name }}" placeholder="{{ isset($placeholders[$name]) ? $placeholders[$name] : '' }}" type="text" id="{{ $name }}" />
                                    @break
                                @endswitch
                            </div>
                            @endforeach
                            <x-jet-button class="mt-4 uppercase" type="submit">Mettre à jour la resource</x-jet-button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </x-jey-modal>
</div>
