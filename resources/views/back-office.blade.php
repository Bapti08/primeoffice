<div>
    <x-primeoffice::toast />
    <div class="flex h-screen overflow-hidden bg-gray-100">

        <div class="hidden md:flex md:flex-shrink-0">
            <div class="flex flex-col w-64">
                <div class="flex flex-col flex-1 h-0">
                    <div class="flex flex-col flex-1 overflow-y-auto">
                        <nav class="flex-1 px-2 py-4 space-y-1 bg-gray-800">
                            @foreach($models as $key => $model)
                                <button wire:click="setModel('{{ $key }}')" class="flex items-center w-full gap-3 px-2 py-2 text-sm font-medium text-white bg-gray-900 rounded-md group">
                                    @isset($icons[$key])
                                        <div class="text-gray-300">{!! $icons[$key] !!}</div>
                                    @endisset
                                    {{ $key }}
                                </button>
                            @endforeach
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="flex flex-col flex-1 w-0 overflow-hidden">
            @livewire('primeoffice::backoffice-search')

            <main class="relative flex-1 overflow-y-auto focus:outline-none" tabindex="0">
                <div class="py-6">
                    <div class="flex items-center justify-between px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
                        <h1 class="text-2xl font-semibold text-gray-900">{{ array_keys($models, $models[$currentModel])[0] }}</h1>
                        <div class="flex gap-3">
                            @if (strpos($this->modes[$this->currentModel], 'c') !== false)
                                <x-jet-button wire:click="create" class="px-3 py-3 text-sm font-semibold text-white uppercase transition-colors bg-blue-600 rounded-md hover:bg-blue-500">
                                    Créer une nouvelle resource
                                </x-jet-button>
                            @endif

                            @livewire('primeoffice::backoffice-filter', [
                                'display' => $display[$currentModel]
                            ])
                        </div>
                    </div>
                    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
                        <div class="py-4">
                            @livewire('primeoffice::backoffice-show', [
                                'model' => $models[$currentModel],
                                'models' => $models,
                                'fields' => $fields[$currentModel],
                                'display' => isset($display[$currentModel]) ? $display[$currentModel] : null,
                                'modes' => $modes[$currentModel],
                            ])
                        </div>
                    </div>
                </div>
            </main>

            {{-- Create resource modal --}}
            @if (strpos($modes[$currentModel], 'c') !== false)

                @livewire('primeoffice::backoffice-create', [
                    'model' => $models[$currentModel],
                    'models' => $models,
                    'fields' => $fields[$currentModel],
                    'display' => isset($display[$currentModel]) ? $display[$currentModel] : null
                ])

            @endif

            {{-- Update resource modal --}}
            @if(strpos($modes[$currentModel], 'u') !== false)

                @livewire('primeoffice::backoffice-update', [
                    'model' => $models[$currentModel],
                    'models' => $models,
                    'fields' => $fields[$currentModel],
                    'display' => isset($display[$currentModel]) ? $display[$currentModel] : null,
                ])

            @endif

        </div>
    </div>

</div>
