<?php

namespace Primeoffice\Http\Livewire;

use Livewire\Component;

class BackofficeFilter extends Component
{
    public $display;

    public $filter = null;
    public $direction = 'asc';

    protected $listeners = [
        'setDisplayable' => 'setDisplay',
        'resetFilter' => 'resetFilter'
    ];

    public function render()
    {
        return view('primeoffice::backoffice-filter');
    }

    public function setDisplay(array $display)
    {
        $this->display = $display;
    }

    public function resetFilter()
    {
        $this->reset(['filter', 'direction']);
    }

    public function setFilter(?string $field = null)
    {
        if ($field == $this->filter) {
            if ($this->direction == 'asc') $this->direction = 'desc';
            else $this->direction = 'asc';
        }
        else $this->direction = 'asc';

        $this->filter = $field;

        $this->emit('filter', $this->filter, $this->direction);
    }
}
