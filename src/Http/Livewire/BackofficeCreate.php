<?php

namespace Primeoffice\Http\Livewire;

use Livewire\Component;
use Primeoffice\Models\ValidationException;
use Primeoffice\Models\Validator;

class BackofficeCreate extends Component
{
    public array $models;
    public string $model;
    public array $fields;
    public array $display;

    protected $listeners = ['create' => 'openCreateModal'];

    public bool $create_modal = false;
    public array $create_fields = [];

    public function render()
    {
        return view('primeoffice::backoffice-create');
    }

    public function openCreateModal(string $model, array $display, array $fields) 
    { 
        $this->model = $model;
        $this->fields = $fields;
        $this->display = $display;
        $this->create_modal = !$this->create_modal; 
    }

    public function store()
    {
        $validator = new Validator($this->create_fields, $this->fields);

        try {
            $validator->validate();

            $model = new $this->model($validator->getData());
            $model->save();

            $this->reset(['create_fields', 'create_modal']);
            $this->emit('content');
            $this->dispatchBrowserEvent('notify', ['title' => 'Resource sauvegardé']);
        }
        catch(ValidationException $e) {
            $this->reset(['create_fields', 'create_modal']);
            $this->dispatchBrowserEvent('notify', ['icon' => 'trash', 'title' => 'Erreur un champ ne correspond pas']);
        }
    }
}
