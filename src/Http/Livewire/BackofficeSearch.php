<?php

namespace Primeoffice\Http\Livewire;

use Livewire\{
    Component,
};

class BackofficeSearch extends Component
{
    public $search = '';

    public function render()
    {
        return view('primeoffice::backoffice-search');
    }

    public function updatedSearch()
    {
        $this->emit('search', $this->search);
    }
}
