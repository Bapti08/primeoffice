<?php

namespace Primeoffice\Http\Livewire;

use Livewire\{
    Component,
    WithFileUploads,
    WithPagination
};

class BackOffice extends Component
{
    use WithFileUploads;

    public $models;
    public $fields;
    public $icons;
    public $modes;
    public $display;

    public $currentModel;

    public function mount()
    {
        $this->models = config('backoffice.models');
        $this->fields = config('backoffice.fields');
        $this->icons = config('backoffice.apparences');
        $this->modes = config('backoffice.modes');
        $this->display = config('backoffice.display');

        $this->currentModel = array_key_first($this->models);
    }

    public function render()
    {
        return view('primeoffice::back-office')->layout('primeoffice::layouts.backoffice');
    }

    public function setModel(string $model)
    {
        $this->currentModel = $model;
        $this->emit('model', $this->models[$this->currentModel], $this->display[$this->currentModel], $this->fields[$this->currentModel], $this->modes[$this->currentModel]);
        $this->emit('setDisplayable', $this->display[$this->currentModel]);
    }

    public function create()
    {
        $this->emit('create', $this->models[$this->currentModel], $this->display[$this->currentModel], $this->fields[$this->currentModel]);
    }
}
