<?php

namespace Primeoffice\Http\Livewire;

use Livewire\{
    Component,
    WithPagination
};

class BackofficeShow extends Component
{
    use WithPagination;

    public string $model;
    public array  $models;
    public array  $display;
    public array  $fields;
    public string $modes;

    private $content;
    public $search;
    public $filter;
    public $filterDirection = 'asc';

    protected $listeners = [
        'content' => '$refresh',
        'model' => 'setModel',
        'search' => 'setContent',
        'filter' => 'setFilter'
    ];

    public function mount()
    {
        $this->content = $this->model::paginate(10);
    }

    public function render()
    {
        if (empty($this->search)) {
            if ($this->filter) $this->content = $this->model::orderBy($this->filter, $this->filterDirection)->paginate(10);
            else $this->content = $this->model::paginate(10);
        }
        else {
            $this->content = $this->model::where(array_key_first($this->fields), 'like', '%' . $this->search . '%');

            $fields = array_keys($this->fields);
            unset($fields[0]);
            foreach ($fields as $field) $this->content->orWhere($field, 'like', '%' . $this->search . '%');

            if ($this->filter) $this->content = $this->content->orderBy($this->filter, $this->filterDirection)->paginate(10);
            else $this->content = $this->content->paginate(10);
        }

        return view('primeoffice::backoffice-show', ['content' => $this->content]);
    }

    public function setContent(?string $search)
    {
        $this->search = $search;
        $this->emitSelf('content');
    }

    public function setFilter(?string $filter, ?string $direction)
    {
        $this->filter = $filter;
        $this->filterDirection = $direction;

        $this->emitSelf('content');
    }

    public function setModel(string $model, array $display, array $fields, string $modes)
    {
        $this->model = $model;
        $this->display = $display;
        $this->modes = $modes;
        $this->fields = $fields;

        $this->reset(['search', 'filter', 'filterDirection']);

        $this->emit('resetFilter');
        $this->emitSelf('content');
    }

    public function update(int $row)
    {
        $this->emit('update', $this->model, $row, $this->display, $this->fields);
    }

    public function delete(int $row)
    {
        if (strpos($this->modes, 'd') !== false) {
            $resource = $this->model::find($row);
            $resource->delete();
            $this->dispatchBrowserEvent('notify', ['title' => 'Resource sauvegardé']);

            $this->emitSelf('content');
        }
    }
}
