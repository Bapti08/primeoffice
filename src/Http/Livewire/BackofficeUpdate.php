<?php

namespace Primeoffice\Http\Livewire;

use Livewire\Component;
use Primeoffice\Models\ValidationException;
use Primeoffice\Models\Validator;

class BackofficeUpdate extends Component
{
    public string $model;
    public array  $models;
    public array  $fields;
    public array  $display;

    public array $update_fields = [];
    public array $enter_fields = [];
    public array $placeholders = [];
    public int   $row = 0;

    protected $listeners = [
        'content' => '$refresh',
        'update' => 'openUpdateModal'
    ];

    public bool $update_modal = false;

    public function render()
    {
        return view('primeoffice::backoffice-update');
    }

    public function openUpdateModal(string $model, int $row, array $display, array $fields)
    {
        $this->row = $row;
        $this->model = $model;
        $this->display = $display;
        $this->fields = $fields;

        $this->enter_fields = $this->fields;
        $this->placeholders = $this->model::find($row)->toArray();
        $this->update_modal = !$this->update_modal;
    }

    public function update()
    {
        foreach ($this->update_fields as $key => $val) if (!isset($this->enter_fields[$key])) {
            unset($this->update_fields[$key]);
        }

        foreach ($this->enter_fields as $key => $val) if (!isset($this->update_fields[$key])) {
            unset($this->enter_fields[$key]);
        }

        $validator = new Validator($this->update_fields, $this->enter_fields);

        try {
            $validator->validate();

            $resource = $this->model::find($this->row);
            $resource->update($validator->getData());

            $this->reset(['update_fields', 'update_modal', 'enter_fields']);
            $this->emit('content');
            $this->dispatchBrowserEvent('notify', ['title' => 'Resource sauvegardé']);
        }
        catch(ValidationException $e) {
            $this->reset(['update_fields', 'update_modal', 'enter_fields']);
            $this->dispatchBrowserEvent('notify', ['icon' => 'trash', 'title' => 'Erreur un champ ne correspond pas']);
        }
    }
}
