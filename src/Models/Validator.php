<?php

namespace Primeoffice\Models;

use ErrorException;
use Exception;

class ValidationException extends Exception {
    protected $message;

    public function __construct($message)
    {
        parent::__construct($message, 500);
    }
}

class Validator
{

    /**
     * Max string lenght accepted
     *
     * @var int
     */
    private const STRLEN = 255;

    /**
     * Datas passed
     *
     * @var array
     */
    private array $datas;

    /**
     * Rules
     *
     * @var array
     */
    private array $rules;

    /**
     * Constructor
     *
     * @param  array $data
     * @param  array $rules
     * @return object
     */
    public function __construct(array $data, array $rules)
    {
        $this->datas = $data;
        $this->rules = $rules;
    }

    /**
     * Validate fields
     *
     * @param  string $field
     * @return int|bool
     */
    public function validate()
    {
        foreach ($this->rules as $key => $val) {
            $rules = explode('.', $val);

            switch ($rules[0]) {
                case 'id':
                    if (gettype($this->datas[$key]) != 'integer' && $this->datas[$key] <= 0 && !$rules[1]::find($this->datas[$key]))
                        throw new ValidationException("erreur");
                break;

                case 'int':
                    if (gettype($this->datas[$key]) != 'integer') throw new ValidationException("erreur");
                break;

                case 'float':
                    if (gettype($this->datas[$key]) != 'double') throw new ValidationException("erreur");
                break;

                case 'string':
                    if (gettype($this->datas[$key]) != 'string' && strlen($this->datas[$key]) > self::STRLEN)
                        throw new ValidationException("erreur");
                break;

                case 'email':
                    if (gettype($this->datas[$key]) != 'string' && strlen($this->datas[$key]) > self::STRLEN) throw new ValidationException("erreur");
                    else {
                        $at_pos = strpos($this->datas[$key], '@');
                        if ($at_pos === false) throw new ValidationException("erreur");
                        else {
                            $domain = substr($this->datas[$key], $at_pos);
                            if (strpos($domain, '.') === false) throw new ValidationException("erreur");
                        }
                    }
                break;

                case 'text':
                    if (gettype($this->datas[$key]) != 'text') throw new ValidationException("erreur");
                break;

                case 'select':
                    unset($rules[0]);
                    $val = explode('|', $rules[1]);
                    if (!in_array($this->datas[$key], $val)) throw new ValidationException("erreur");
                break;

                case 'color':
                    $val = explode('-', $this->datas[$key]);
                    if ($val[1] > 900 || $val[1] < 100) throw new ValidationException("erreur");
                break;

                case 'file':
                    // blob ?
                break;

                case 'bool':
                    if (gettype($this->datas[$key]) != 'integer' && gettype($this->datas[$key]) != 'boolean') throw new ValidationException("erreur");
                break;

                case 'crypt':
                    if (gettype($this->datas[$key]) != 'string') throw new ValidationException("erreur");
                    else $this->datas[$key] = bcrypt($this->datas[$key]);
                break;

                default:
                    throw new ValidationException('Les données ne sont pas corrects');
                break;
            }
        }

        return true;
    }

    /**
     * Validate only one field
     *
     * @param  string $field
     * @return int|bool
     */
    public function validateOnly(string $field)
    {
        $rules = explode('.', $this->rules[$field]);

        switch ($rules[0]) {
            case 'id':
                if (gettype($this->datas[$field]) != 'integer' && $this->datas[$field] <= 0 && !$rules[1]::find($this->datas[$field]))
                    return $field;
            break;

            case 'int':
                if (gettype($this->datas[$field]) != 'integer') return $field;
            break;

            case 'float':
                if (gettype($this->datas[$field]) != 'double') return $field;
            break;

            case 'string':
                if (gettype($this->datas[$field]) != 'string' && strlen($this->datas[$field]) > self::STRLEN)
                    return $field;
            break;

            case 'text':
                if (gettype($this->datas[$field]) != 'string') return $field;
            break;

            case 'select':
                unset($rules[0]);
                if (!in_array($this->datas[$field], $rules)) return $field;
            break;

            default:
                // throw exception
            break;
        }

        return true;
    }

    public function getData()
    {
        return $this->datas;
    }
}
