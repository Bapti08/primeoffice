<?php

namespace Primeoffice;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Primeoffice\Http\Middleware\AdminMiddleware;

use Primeoffice\Http\Livewire\{
    BackOffice,
    BackofficeCreate,
    BackofficeFilter,
    BackofficeShow,
    BackofficeUpdate,
    BackofficeSearch
};

class PrimeofficeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/backoffice.php' => config_path('backoffice.php'),
            //__DIR__ . '/../resources/views/layouts' => resource_path('views/vendor/primeoffice'),
        ], 'primeoffice');

        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('auth.admin', AdminMiddleware::class);

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'primeoffice');

        Livewire::component('primeoffice::back-office', BackOffice::class);
        Livewire::component('primeoffice::backoffice-create', BackofficeCreate::class);
        Livewire::component('primeoffice::backoffice-update', BackofficeUpdate::class);
        Livewire::component('primeoffice::backoffice-show', BackofficeShow::class);
        Livewire::component('primeoffice::backoffice-search', BackofficeSearch::class);
        Livewire::component('primeoffice::backoffice-filter', BackofficeFilter::class);
    }
}
